﻿using System;
using UnityEngine;


[RequireComponent(typeof(MeleeWeapon))]
[RequireComponent(typeof(HorizontalMovement))]
public class EnemyController : MonoBehaviour
{

    [SerializeField] private float m_movementRange = 5;
    
    private MeleeWeapon m_mWeapon;
    private HorizontalMovement m_movement;

    private Vector2[] m_wayPoints;
    private int m_currWayPointIndex;
    
    private void Start()
    {
        m_wayPoints = new Vector2[]
        {
            transform.position,
            transform.position + Vector3.left * m_movementRange
        };
        
        m_mWeapon = GetComponent<MeleeWeapon>();
        m_movement = GetComponent<HorizontalMovement>();
    }

    private void FixedUpdate()
    {
        if (IsCanAttack())
        {
            m_mWeapon.Attack();
            return;
        }

        MoveThrowWaypoints();
    }

    private void MoveThrowWaypoints()
    {
        if (Math.Abs(m_movementRange) < float.Epsilon)
        {
            return;
        }
        
        var point = m_wayPoints[m_currWayPointIndex];
        var moveDir = point.x > transform.position.x ? 1 : -1;
        
        m_movement.Move(moveDir);

        var frameSpeed = m_movement.Speed * Time.deltaTime;
        if (Mathf.Abs(point.x - transform.position.x) < frameSpeed)
        {
            m_currWayPointIndex = m_currWayPointIndex == 0 ? 1 : 0;
        }
    }

    private bool IsCanAttack()
    {
        var origin = transform.position;
        var direction = transform.right;
        var range = m_mWeapon.Data.Range;

        var hit = Physics2D.Raycast(origin, direction, range, m_mWeapon.Data.Mask);
        if (hit.collider != null)
        {
            var player = hit.collider.GetComponent<PlayerController>();
            if (player != null)
            {
                return true;
            }
        }

        return false;
    }

    private void OnDrawGizmosSelected()
    {
        var point1 = transform.position;    
        var point2 = transform.position + Vector3.left * m_movementRange;

        if (Application.isPlaying)
        {
            point1 = m_wayPoints[0];
            point2 = m_wayPoints[1];
        }
        
        point1.y += 0.5f;
        point2.y += 0.5f;
        
        Gizmos.DrawSphere(point1, 0.2f);
        Gizmos.DrawSphere(point2, 0.2f);
        Gizmos.DrawLine(point1, point2);
    }
}