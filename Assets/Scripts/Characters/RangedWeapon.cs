﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class RangedWeapon : MonoBehaviour
{
	[SerializeField] private RangeWeaponData m_weapon;
	[SerializeField] private Transform m_firePoint;

	private bool m_isReloading;

	public UnityEvent OnShot = new UnityEvent();
    
	public void Attack()
	{ 
		if(!enabled) return;
		
		if (m_isReloading)
		{
			return;
		}
        
		StartCoroutine(ProcessAttack());
	}

	private IEnumerator ProcessAttack()
	{
		m_isReloading = true;
		OnShot.Invoke();
       
		m_weapon.Attack(m_firePoint);
        
		yield return new WaitForSeconds(m_weapon.FireRate);

		m_isReloading = false;
	}
}