﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class FreezOnHit : MonoBehaviour
{
    [SerializeField] private float m_freezeTime = 1;

    private Health m_hp;
    
    private void Start()
    {
        m_hp = GetComponent<Health>();
        m_hp.OnHpChanged.AddListener(Freeze);
    }

    private void OnDestroy()
    {
        m_hp.OnHpChanged.RemoveListener(Freeze);
    }

    private void Freeze()
    {
        if(m_hp.HP <= 0) return;
        StartCoroutine(FreezeCoroutine());
    }

    private IEnumerator FreezeCoroutine()
    {
        EnableComponent<HorizontalMovement>(false);
        EnableComponent<Jumping>(false);
        EnableComponent<MeleeWeapon>(false);
        EnableComponent<RangedWeapon>(false);
        
        yield return new WaitForSeconds(m_freezeTime);
        
        EnableComponent<HorizontalMovement>(true);
        EnableComponent<Jumping>(true);
        EnableComponent<MeleeWeapon>(true);
        EnableComponent<RangedWeapon>(true);
    }

    private void EnableComponent<T>(bool enable) where T : MonoBehaviour
    {
        var comp = GetComponent<T>();
        if (comp != null)
        {
            comp.enabled = enable;
        }
    }
}