﻿using UnityEngine;

[RequireComponent(typeof(HorizontalMovement))]
[RequireComponent(typeof(Jumping))]
[RequireComponent(typeof(MeleeWeapon))]
[RequireComponent(typeof(RangedWeapon))]
public class PlayerController : MonoBehaviour
{

    private HorizontalMovement m_movement;
    private Jumping m_jump;
    private MeleeWeapon m_meleeWeapon;
    private RangedWeapon m_rangedWeapon;

    
    private void Start()
    {
        m_movement = GetComponent<HorizontalMovement>();
        m_jump = GetComponent<Jumping>();
        m_meleeWeapon = GetComponent<MeleeWeapon>();
        m_rangedWeapon = GetComponent<RangedWeapon>();
    }

    private void FixedUpdate()
    {
        m_movement.Move(Input.GetAxis("Horizontal"));
    }

    private void Update()
    {
       
        if (Input.GetButtonDown("Jump"))
        {
            m_jump.Jump();
        }

        if (Input.GetButtonDown("Fire2"))
        {
            m_meleeWeapon.Attack();
        }
        
        if (Input.GetButton("Fire1"))
        {
            m_rangedWeapon.Attack();
        }
    }
}