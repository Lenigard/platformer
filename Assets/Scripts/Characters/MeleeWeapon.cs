﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class MeleeWeapon : MonoBehaviour
{
    [SerializeField] private MeleeWeaponData m_weapon;
    [SerializeField] private Transform m_firePoint;
    
    public bool IsAttacking { get; private set; }

    public UnityEvent AttackStarted = new UnityEvent();
    public UnityEvent AttackCompleted = new UnityEvent();
    
    public MeleeWeaponData Data
    {
        get { return m_weapon; }
    }
    
    public void Attack()
    { 
        if(!enabled) return;
        
        if (IsAttacking)
        {
            return;
        }
        
        StartCoroutine(ProcessAttack());
    }

    private IEnumerator ProcessAttack()
    {
        IsAttacking = true;
        AttackStarted.Invoke();
        
        yield return new WaitForSeconds(m_weapon.HitDelay);
        
        m_weapon.Attack(m_firePoint);
        
        yield return new WaitForSeconds(m_weapon.FireRate);

        IsAttacking = false;
        AttackCompleted.Invoke();
    }

    private void OnDrawGizmosSelected()
    {
        if (m_firePoint != null && m_weapon != null)
        {
            var origin = m_firePoint.position;
            var direction = m_firePoint.right * m_weapon.Range;
            Gizmos.color = Color.red;
            Gizmos.DrawRay(origin, direction);
        }
    }
}