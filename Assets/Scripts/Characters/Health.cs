﻿using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    [SerializeField] private float m_health = 1f;
    [SerializeField] private float m_destroyDelay = 1f;

    public UnityEvent OnDie = new UnityEvent();
    public UnityEvent OnHpChanged = new UnityEvent();

    public float HP
    {
        get { return m_health; }
    }
    
    public void ApplyDamage(float damage)
    {
        m_health -= damage;
        OnHpChanged.Invoke();
        
        if (m_health <= 0)
        {
            DestroyComponent<PlayerController>();   
            DestroyComponent<EnemyController>();   
            DestroyComponent<HorizontalMovement>();
            DestroyComponent<Jumping>();   
            DestroyComponent<Rigidbody2D>();  
            DestroyComponent<Collider2D>();
            
            Destroy(gameObject, m_destroyDelay);
            
            OnDie.Invoke();
        }
    }

    private void DestroyComponent<T>() where T : Component
    {
        var component = GetComponent<T>();
        if (component != null)
        {
            Destroy(component);
        }
    }
}