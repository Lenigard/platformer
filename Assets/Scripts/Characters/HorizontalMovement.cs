﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class HorizontalMovement : MonoBehaviour
{
    [SerializeField] private float m_speed = 5f;
    
    private Rigidbody2D m_rb2D;
  
    public Vector2 Velocity
    {
        get { return m_rb2D.velocity; }
    }

    public float Speed
    {
        get { return m_speed; }
    }

    private void OnEnable()
    {
        if (m_rb2D == null)
        {
            m_rb2D = GetComponent<Rigidbody2D>();
        }
    }

    public void Move(float horizontalAxis)
    {
        if(!enabled) return;
        
        var velocity = m_rb2D.velocity;
        velocity.x = horizontalAxis * m_speed;
        m_rb2D.velocity = velocity; 
        
        if (Math.Abs(horizontalAxis) > 0.0001f)
        {
            var rot = horizontalAxis > 0 ? 0 : 180;
            transform.rotation = Quaternion.Euler(0, rot, 0);
        }
    }
}