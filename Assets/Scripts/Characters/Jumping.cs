﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Jumping : MonoBehaviour
{
    [Header("Jumping:")]
    [SerializeField] private float m_jumpForce;
    
    [Header("Grounding:")]
    [SerializeField] private LayerMask m_mask;
    [SerializeField] private float m_rayDistance = 0.2f;
    [SerializeField] private float m_groundingDelay = 0.2f;

    private Collider2D m_collider;
    private Rigidbody2D m_rb2D;

    private float m_currForce;
    private float m_timeSinceJumpStarted;
    
    public bool IsGrounded { get; private set; }
     
    private void Start()
    {
        m_rb2D = GetComponent<Rigidbody2D>();
        m_collider = GetComponent<Collider2D>();
        m_timeSinceJumpStarted = m_groundingDelay;
    }

    private void FixedUpdate()
    {
        m_timeSinceJumpStarted += Time.fixedDeltaTime;
        CheckGrounding();
    }
    
    private void CheckGrounding()
    {
        //Задержка перед проверкой IsGround
        //Нужна для того чтобы не случилось слишком быстрого перехода из прыжка в приземление
        //Т.к. IsGrounding = true может быть в течении первых нескольких кадров прыжка
        if (m_timeSinceJumpStarted < m_groundingDelay || m_rb2D.velocity.y > 0)
        {
            return;
        }
        
        //Дулаем рейкаст из нижней центральной точки коллайдера
        var y = m_collider.bounds.min.y;
        var x = m_collider.bounds.center.x;

        var hit = Physics2D.Raycast(new Vector2(x, y), Vector2.down, m_rayDistance, m_mask);
        IsGrounded = hit.collider != null;
    }
    
    public void Jump()
    {
        if(!enabled) return;
        
        if (!IsGrounded) return;
        IsGrounded = false;

        m_timeSinceJumpStarted = 0;
        
        m_rb2D.AddForce(Vector2.up * m_jumpForce, ForceMode2D.Impulse);
    }
}