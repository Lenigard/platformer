﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InventoryController))]
[RequireComponent(typeof(MeleeWeapon))]
[RequireComponent(typeof(RangedWeapon))]
public class WeaponActivator : MonoBehaviour
{
    private InventoryController m_inventoryController;
    private MeleeWeapon m_meleeWeapon;
    private RangedWeapon m_rangedWeapon;

    [SerializeField] private InventoryItem m_requiredMeleeItem;
    [SerializeField] private InventoryItem m_requiredRangedItem;

    private void Start()
    {
        m_inventoryController = GetComponent<InventoryController>();
        m_meleeWeapon = GetComponent<MeleeWeapon>();
        m_rangedWeapon = GetComponent<RangedWeapon>();

        m_meleeWeapon.enabled = m_inventoryController.HasItems(m_requiredMeleeItem);
        m_rangedWeapon.enabled = m_inventoryController.HasItems(m_requiredRangedItem);

        m_inventoryController.OnItemAdd.AddListener(OnItemAdded);
        m_inventoryController.OnItemRemove.AddListener(OnItemRemove);
    }

    private void OnDestroy()
    {
        m_inventoryController.OnItemAdd.RemoveListener(OnItemAdded);
        m_inventoryController.OnItemRemove.RemoveListener(OnItemRemove);
    }

    private void OnItemAdded(InventoryItem item)
    {
        if(m_requiredMeleeItem == item)
        {
            m_meleeWeapon.enabled = true;
        }

        if (m_requiredRangedItem == item)
        {
            m_rangedWeapon.enabled = true;
        }
    }

    private void OnItemRemove(InventoryItem item)
    {
        if (m_requiredMeleeItem == item)
        {
            m_meleeWeapon.enabled = false;
        }

        if (m_requiredRangedItem == item)
        {
            m_rangedWeapon.enabled = false;
        }
    }
}
