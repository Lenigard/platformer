﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory/Inventory")]
public class InventoryAsset : ScriptableObject, ISerializationCallbackReceiver
{
    [SerializeField] private InventoryItem[] m_defaultItems;

    [NonSerialized] private List<InventoryItem> m_runtimeItems = new List<InventoryItem>();

    public bool HasItems(params InventoryItem[] items)
    {
        foreach (var item in items)
        {
            if (!m_runtimeItems.Contains(item))
            {
                return false;
            }
        }

        return true;
    }

    public void Add(InventoryItem item)
    {
        if (!m_runtimeItems.Contains(item))
        {
            m_runtimeItems.Add(item);
        }
    }

    public void Remove(InventoryItem item)
    {
        if (m_runtimeItems.Contains(item))
        {
            m_runtimeItems.Remove(item);
        }
    }

    public void OnAfterDeserialize()
    {
        m_runtimeItems.Clear();
        m_runtimeItems.AddRange(m_defaultItems);
    }

    public void OnBeforeSerialize()
    {

    }

    public IEnumerable GetEnumerator()
    {
        foreach (var item in m_runtimeItems)
        {
            yield return item;
        }
    }
}
