﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class PickupableItem : MonoBehaviour
{
    [SerializeField] private InventoryItem m_item;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var inventory = collision.GetComponent<InventoryController>();

        if (inventory != null)
        {
            inventory.AddItem(m_item);
            Destroy(gameObject);
        }
    }
}
