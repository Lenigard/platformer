﻿using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class UnityInventoryItemEvent : UnityEvent<InventoryItem>
{

}

public class InventoryController : MonoBehaviour
{
    public UnityInventoryItemEvent OnItemAdd = new UnityInventoryItemEvent();
    public UnityInventoryItemEvent OnItemRemove = new UnityInventoryItemEvent();

    [SerializeField] private InventoryAsset m_inventory;

    public void AddItem(InventoryItem item)
    {
        m_inventory.Add(item);
        OnItemAdd.Invoke(item);
    }

    public void RemoveItem(InventoryItem item)
    {
        m_inventory.Remove(item);
        OnItemRemove.Invoke(item);
    }

    public bool HasItems(params InventoryItem[] items)
    {
        return m_inventory.HasItems(items);
    }
}
