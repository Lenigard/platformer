﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private float m_loadDelay = 1.5f;

    private static string m_nextScene;
    private static bool m_isAsync;

    private const string LOADING_SCENE = "Loading";

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(m_loadDelay);

        if (string.IsNullOrEmpty(m_nextScene))
        {
            yield break;
        }

        if (!m_isAsync)
        {
            SceneManager.LoadScene(m_nextScene);
            yield break;
        }

        var loading = SceneManager.LoadSceneAsync(m_nextScene, LoadSceneMode.Additive);
        while (!loading.isDone)
        {
            yield return null;
        }

        SceneManager.UnloadSceneAsync(LOADING_SCENE);
    }

    public static void LoadScene(string name, bool isAsync = true)
    {
        m_nextScene = name;
        m_isAsync = isAsync;
        SceneManager.LoadScene(LOADING_SCENE);
    }
}

