﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class Collider2DEvent : UnityEvent<Collider2D>{}

public class OnTriggerEnterComponent : MonoBehaviour
{
    public Collider2DEvent OnTriggerEnterEvent = new Collider2DEvent();
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        OnTriggerEnterEvent.Invoke(other);
    }
}