﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AEnvironmentTrigger : MonoBehaviour
{
    public abstract void OnTriggerEnter2D(Collider2D collision);

    public abstract void MessageTrigger();

    public abstract void Trigger();
}
