﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class DoorTrigger : AEnvironmentTrigger
{
    [SerializeField] private InventoryItem[] m_requiredItems;
    [SerializeField] bool m_shouldItemsBeDestroyed;

    public override void MessageTrigger()
    {
        Debug.Log("Message is triggered");
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        MessageTrigger();
        var inventory = collision.GetComponent<InventoryController>();

        if (inventory.HasItems(m_requiredItems))
        {
            Trigger();
            if (m_shouldItemsBeDestroyed)
            {
                foreach (var item in m_requiredItems)
                {
                    inventory.RemoveItem(item);
                }
            }
        }
    }

    public override void Trigger()
    {
        Destroy(gameObject);
    }

}
