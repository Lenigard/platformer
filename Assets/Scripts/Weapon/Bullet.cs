﻿using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float Damage { get; set; }

    private void OnCollisionEnter2D(Collision2D other)
    {
        var hp = other.collider.GetComponent<Health>();
        if (hp != null)
        {
            hp.ApplyDamage(Damage);
        }
        
        Destroy(gameObject);
    }
}