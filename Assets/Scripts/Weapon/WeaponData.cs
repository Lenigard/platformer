﻿using UnityEngine;

public abstract class WeaponData : ScriptableObject
{
    public LayerMask Mask;
    public float Damage = 1f;
    public float FireRate = .5f;

    public abstract void Attack(Transform hitPoint);
}