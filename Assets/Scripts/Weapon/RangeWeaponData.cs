﻿using UnityEngine;

[CreateAssetMenu]
public class RangeWeaponData : WeaponData
{
    [SerializeField] private Bullet m_ammo;
    [SerializeField] private float m_bulletSpeed = 5;
    [SerializeField] private float m_destroyBulletTime;
    
    public override void Attack(Transform hitPoint)
    {
        var instance = Instantiate(m_ammo, hitPoint.position, hitPoint.rotation);
        instance.Damage = Damage;

        var rb2D = instance.GetComponent<Rigidbody2D>();
        rb2D.gravityScale = 0;
        rb2D.velocity = hitPoint.right * m_bulletSpeed;
        
        Destroy(instance.gameObject, m_destroyBulletTime);
    }
}