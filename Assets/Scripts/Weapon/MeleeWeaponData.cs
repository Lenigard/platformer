﻿using UnityEngine;

[CreateAssetMenu]
public class MeleeWeaponData : WeaponData
{
    public float Range = 2f;
    public float HitDelay = 0.1f;
    
    public override void Attack(Transform hitPoint)
    {
        var origin = hitPoint.position;
        var direction = hitPoint.right;

        var hit = Physics2D.Raycast(origin, direction, Range, Mask);
        if (hit.collider != null)
        {
            var hp = hit.collider.GetComponent<Health>();
            if (hp == null) return;

            hp.ApplyDamage(Damage);
        }
    }
}