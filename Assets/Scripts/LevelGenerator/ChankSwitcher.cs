﻿using UnityEngine;

public class ChankSwitcher : MonoBehaviour
{
    [SerializeField] private LevelChunk[] m_chunks;

    private void Start()
    {
        m_chunks = GetComponentsInChildren<LevelChunk>();
        
        for (var i = 0; i < m_chunks.Length; i++)
        {
            m_chunks[i].Index = i;
            m_chunks[i].OnChunkEnter.AddListener(OnChunkEnter);
            m_chunks[i].OnChunkExit.AddListener(OnChunkExit);
            
            m_chunks[i].gameObject.SetActive(i < 2);
        }
    }

    private void OnDestroy()
    {
        for (var i = 0; i < m_chunks.Length; i++)
        {
            m_chunks[i].OnChunkEnter.RemoveListener(OnChunkEnter);
            m_chunks[i].OnChunkExit.RemoveListener(OnChunkExit);
        }
    }

    private void OnChunkEnter(int index)
    {
        var chunkToEnable = index - 1;
        var chunkToDisable = index + 2;

        if (chunkToEnable >= 0)
        {
            m_chunks[chunkToEnable].gameObject.SetActive(true);
        }
        
        if (chunkToDisable < m_chunks.Length)
        {
            m_chunks[chunkToDisable].gameObject.SetActive(false);
        }
    }
    
    private void OnChunkExit(int index)
    {
        var chunkToEnable = index + 2;
        var chunkToDisable = index - 1;

        if (chunkToEnable < m_chunks.Length)
        {
            m_chunks[chunkToEnable].gameObject.SetActive(true);
        }
        
        if (chunkToDisable >= 0)
        {
            m_chunks[chunkToDisable].gameObject.SetActive(false);
        }
    }
}