﻿using UnityEngine;

[CreateAssetMenu]
public class ChunkSet : ScriptableObject
{
    public enum RepeatType
    {
        Repeat,
        Random
    }

    [SerializeField] [Range(2, 100)] public int Size;
    [SerializeField] private RepeatType m_repeat = RepeatType.Repeat;
    [SerializeField] private LevelChunk m_firstChunk = null;
    [SerializeField] private LevelChunk m_lastChunk = null;
    [SerializeField] private LevelChunk[] m_chunks = new LevelChunk[0];

    public LevelChunk GetNextChunk(int i)
    {
        LevelChunk result = null;

        if (i == 0)
        {
            result = m_firstChunk;
        }
        else if (i == Size - 1)
        {
            result = m_lastChunk;
        }
        else
        {
            if (m_repeat == RepeatType.Random)
            {
                result =  GetRndChunk();
            }
            else if(m_repeat == RepeatType.Repeat)
            {
                result = GetRepeatChunk(i);
            }
            
        }

        return result;
    }

    private LevelChunk GetRndChunk()
    {
        var rndIndex = Random.Range(0, m_chunks.Length);
        return m_chunks[rndIndex];
    }

    private LevelChunk GetRepeatChunk(int i)
    {
        i = (int)Mathf.Repeat(i - 1, m_chunks.Length);
        return m_chunks[i];
    }
}