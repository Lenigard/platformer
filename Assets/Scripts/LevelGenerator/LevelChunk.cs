﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

[Serializable]
public class UnityIntEvent : UnityEvent<int>{}

[RequireComponent(typeof(Tilemap))]
[RequireComponent(typeof(BoxCollider2D))]
[ExecuteInEditMode]
public class LevelChunk : MonoBehaviour
{
    public int Index { get; set; }

    private Tilemap m_map;
    private BoxCollider2D m_trigger;

    public UnityIntEvent OnChunkEnter = new UnityIntEvent();
    public UnityIntEvent OnChunkExit = new UnityIntEvent();

    private Tilemap Map
    {
        get
        {
            if (m_map == null)
            {
                m_map = GetComponent<Tilemap>();
            }
            return m_map;
        }
    }
    
    public int Width
    {
        get { return Map.size.x; }
    }
    
    public float OffsetX
    {
        get { return Map.cellBounds.center.x; }
    }
    
    private void Start()
    {
        m_map = GetComponent<Tilemap>();
        m_trigger = GetComponent<BoxCollider2D>();
        m_trigger.isTrigger = true;

        SetTriggerSizeAndOffset();
    }

    private void SetTriggerSizeAndOffset()
    {
        var xOffset = m_map.size.x / 2f + m_map.cellBounds.center.x - 0.5f;
        var yOffset = m_map.cellBounds.center.y;
        m_trigger.offset = new Vector2(xOffset, yOffset);
        m_trigger.size = new Vector2(1, m_map.size.y);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var player = other.GetComponent<PlayerController>();
        if (player == null)
        {
            return;
        }
        
        var centerX = transform.position.x + m_trigger.offset.x;

        if (other.transform.position.x > centerX)
        {
            OnChunkExit.Invoke(Index);
        }

        if (other.transform.position.x < centerX)
        {
            OnChunkEnter.Invoke(Index);
        }
    }

    [ContextMenu("Reset trigger")]
    public void ResetTrigger()
    {
        SetTriggerSizeAndOffset();
    }
}