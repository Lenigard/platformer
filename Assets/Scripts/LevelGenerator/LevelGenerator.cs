﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private ChunkSet m_set;
    
    private void Awake()
    {
        GenerateLevel();
    }

    [ContextMenu("Generate")]
    public void GenerateLevel()
    {
        if (m_set == null)
        {
            return;
        }

        var pos = Vector3.zero;
        for (int i = 0; i < m_set.Size; i++)
        {
            var chunk = m_set.GetNextChunk(i);
            var offset = Vector3.right * (chunk.Width) * 0.5f;
            
            if (i != 0)
            {
                //Because center of TileMap can be bot in center of tiles
                pos += offset - new Vector3(chunk.OffsetX, 0, 0);
            }
            
            Instantiate(chunk, pos, Quaternion.identity, transform);
            pos += offset + new Vector3(chunk.OffsetX, 0, 0);
        }
    }
}