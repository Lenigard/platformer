﻿using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Platform2D : MonoBehaviour
{
    [SerializeField] private Vector2[] m_waypoints;
    [SerializeField] private float m_speed = 3f;
    [SerializeField] private float m_catchDelta = 0.1f;

    private Vector3 m_startPoint;
    private Rigidbody2D m_rb2D;
    private Collider2D m_collider;
    private ContactPoint2D[] m_contacts = new ContactPoint2D[8];

    private void Start()
    {

        m_rb2D = GetComponent<Rigidbody2D>();
        m_collider = GetComponent<Collider2D>();
        m_startPoint = transform.position;
        
        var path = new Vector3[m_waypoints.Length];
        for (int i = 0; i < m_waypoints.Length; i++)
        {
            path[i] = transform.position + (Vector3)m_waypoints[i];
        }

        transform.DOPath(path, m_speed)
            .SetSpeedBased(true)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Yoyo);
    }

    private void FixedUpdate()
    {
        ClearContacts();
        MoveContacts();
    }

    private void MoveContacts()
    {
        m_rb2D.GetContacts(m_contacts);
        foreach (var contact in m_contacts)
        {
            if (contact.rigidbody == null) continue;
            if (contact.rigidbody.position.y < m_rb2D.position.y) continue;
            if (contact.point.y - m_collider.bounds.max.y > m_catchDelta) continue;
            contact.rigidbody.transform.SetParent(transform);
        }
    }

    private void ClearContacts()
    {
        for (int i = 0; i < m_contacts.Length; i++)
        {
            if (m_contacts[i].rigidbody != null)
            {
                m_contacts[i].rigidbody.transform.SetParent(null);
            }

            m_contacts[i] = new ContactPoint2D();
        }
    }

    private void OnDrawGizmosSelected()
    {
        if(m_waypoints == null) return;

        var startPoint = transform.position;
        
        if (Application.isPlaying)
        {
            startPoint = m_startPoint;
        }
        
        for (int i = 0; i < m_waypoints.Length - 1; i++)
        {
            var start = (Vector2)startPoint + m_waypoints[i];
            var end = (Vector2)startPoint + m_waypoints[i + 1];
            Gizmos.DrawSphere(start, 0.2f);
            Gizmos.DrawSphere(end, 0.2f);
            Gizmos.DrawLine(start, end);
        }
    }
}