﻿using UnityEngine;

[CreateAssetMenu]
public class LoadSceneCmd : ScriptableObject
{
   public void LoadScene(string name)
    {
        SceneLoader.LoadScene(name);
    }
}
