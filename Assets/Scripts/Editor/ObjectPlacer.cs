﻿using UnityEditor;
using UnityEngine;

public class ObjectsPlacer : MonoBehaviour
{
    //%#F6 - is hotkey
    //https://docs.unity3d.com/ScriptReference/MenuItem.html
    
    [MenuItem("Tools/Place to ground %#F6")]
    private static void PlaceToGround()
    {
        var objects = Selection.gameObjects;
        foreach (var obj in objects)
        {
            PlaceObject(obj);
        }
    }

    private static void PlaceObject(GameObject obj)
    {
        var colliders = obj.GetComponents<Collider2D>();
        if (colliders.Length == 0)
        {
            return;
        }

        var minY = float.MaxValue;
        foreach (var collider in colliders)
        {
            if (collider.bounds.min.y < minY)
            {
                minY = collider.bounds.min.y;
            }
        }

        var origin = obj.transform.position;
        origin.y = minY - 0.01f;
        
        var hit = Physics2D.Raycast(origin, Vector2.down);
        
        if (hit.collider == null)
        {
            return;
        }
        
        var pos = hit.point;        
        pos.y += obj.transform.position.y - minY;
        obj.transform.position = pos;
    }
}