﻿using UnityEngine;

public class SMB_Attacking : StateMachineBehaviour
{
    private Animator m_animator;
    private MeleeWeapon m_weapon;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        m_weapon = animator.GetComponentInParent<MeleeWeapon>();

        if (m_weapon != null)
        {
            m_animator = animator;
            m_weapon.AttackStarted.AddListener(OnAttack);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (m_weapon != null) m_weapon.AttackStarted.RemoveListener(OnAttack);
    }

    private void OnAttack()
    {
        m_animator.SetTrigger("Attack");
    }
}