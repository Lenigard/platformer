﻿using UnityEngine;

public class SMB_Die : StateMachineBehaviour
{
    private Health m_health;
    private Animator m_animator;
    
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_health == null)
        {
            m_animator = animator;  
            m_health = animator.GetComponentInParent<Health>();     
            m_health.OnDie.AddListener(OnDie);
        }
    }

    private void OnDestroy()
    {
        if (m_health != null)
        {
            m_health.OnDie.RemoveListener(OnDie);
        }
    }

    private void OnDie()
    {
        m_animator.SetTrigger("Die");
    }
}
