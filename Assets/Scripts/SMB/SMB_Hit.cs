﻿using UnityEngine;
using UnityEngine.Animations;

public class SMB_Hit : StateMachineBehaviour
{
    private Animator m_animator;
    private Health m_hp;
    
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex,  AnimatorControllerPlayable controller)
    {
        if(m_hp != null) return;
        
        m_hp = animator.gameObject.GetComponentInParent<Health>();
        if (m_hp == null) return;
        
        m_animator = animator;    
        m_hp.OnHpChanged.AddListener(OnHpChanged);
    }

    private void OnDestroy()
    {
        if (m_hp != null)
        {
            m_hp.OnHpChanged.RemoveListener(OnHpChanged);
        }
    }

    private void OnHpChanged()
    {
        if (m_hp.HP > 0)
        {
            m_animator.SetTrigger("Hit");
        }
    }
}