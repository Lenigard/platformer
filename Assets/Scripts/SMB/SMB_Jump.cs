﻿using UnityEngine;

public class SMB_Jump : StateMachineBehaviour 
{
    private Jumping m_jump;
    
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (m_jump == null)
        {
            m_jump = animator.GetComponentInParent<Jumping>();
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (m_jump == null)
        {
            return;
        }
        
        animator.SetBool("IsGrounded", m_jump.IsGrounded);
    }
}
