﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMB_Movement : StateMachineBehaviour
{
    private HorizontalMovement m_movement;
    
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (m_movement == null)
        {
            m_movement = animator.GetComponentInParent<HorizontalMovement>();
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (m_movement == null)
        {
            return;
        }
        
        animator.SetFloat("Speed", m_movement.Velocity.magnitude);
    }
}